// Rollup plugins
import babel from 'rollup-plugin-babel';
import eslint from 'rollup-plugin-eslint';
import replace from 'rollup-plugin-replace';
import uglify from 'rollup-plugin-uglify-es';
import sass from 'node-sass';
import postcss from 'rollup-plugin-postcss';

// PostCSS plugins
import nested from 'postcss-nested';
import cssnext from 'postcss-cssnext';
import cssnano from 'cssnano';

import pkg from './package.json';

export default [
	{
		input: 'src/js/index.js',
		sourcemap: true,
		output: [
			{
				file: 'docs/assets/js/svg-animation.min.js',
				format: 'iife',
			},
			{
				file: pkg.browser,
				format: 'iife',
			}
		],
		name: 'Animate',
		plugins: [
			postcss({
				preprocessor: (content, id) => new Promise((resolve, reject) => {
					const result = sass.renderSync({ file: id })
					resolve({ code: result.css.toString() })
				}),
				extensions: [ '.scss' ],
				plugins: [
					nested(),
					cssnext({ warnForDuplicates: false }),
					cssnano()
				]
			}),
			eslint({
				exclude: [
					'src/scss/**'
				]
			}),
			babel({
				exclude: 'node_modules/**'
			}),
			replace({
				exclude: 'node_modules/**',
				ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
			}),
			uglify()
		]
	}
];
