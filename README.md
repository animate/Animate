<div align="center">
    <img src="docs/assets/img/logo.svg" height="85px">
    <p align="center">
        <p>Animate for the web with ease</p>
    </p>
</div>

# Usage

Animate is a tool to add animations to your static svg, with a simple and easy to use interface.
To get started just import the svg which has the objects that you need to animate.
Add the duration and number of frames, Click on the wmpty frame to insert a key frame, add an end key frame.
Select the object on the canvas and move transform, and scale it accordingly.

Animate will create a interpolated (tweened) animation for the object.

To view the the resulting animation just press play in the control bar.

If you are happy with the animation, save the svg by exporting it.

# License
GPL 3.0
