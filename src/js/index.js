
function importSVG() {
	//Check
}

function parseSVG() {

}

function parse () {

}

function getAttribute() {

}

// function generateAnimElement(attributeName, keyTimes, dur, repeatCount, values) {

// }

function attachAnim(element, props, dur, noOfFrames) {

	for(var attributeName in props) {
		let animateElem = document.createElementNS("http://www.w3.org/2000/svg", "animate");
		let values = props[attributeName];

		let seconds = dur/1000;
		let timespan = seconds/noOfFrames;
		let keyTimes = [];

		// for(var i = 0; i <= noOfFrames; i++) {
		// 	keyTimes.push(i * );
		// }

		// let currentValue = oldValues[attributeName] || element.getAttribute(attributeName);
		let animAttr = {
			attributeName: attributeName,
			// from: currentValue,
			// to: value,
			// begin: "0s",
			dur: seconds + "s",
			repeatCount: 1,
			// keyTimes: ,
			// values: currentValue + ";" + value,
			// keySplines: EASING[easingType],
			values: values.join(';')
			// calcMode: "spline",
			// fill: 'freeze'
		};

		for (var i in animAttr) {
			animateElement.setAttribute(i, animAttr[i]);
		}

		element.appendChild(animateElement);
	}

}

const EASING = {
	ease: "0.25 0.1 0.25 1",
	linear: "0 0 1 1",
	// easein: "0.42 0 1 1",
	easein: "0.1 0.8 0.2 1",
	easeout: "0 0 0.58 1",
	easeinout: "0.42 0 0.58 1"
};



function animateSVGElement(element, props, dur, easingType="linear", type=undefined, oldValues={}) {

	// [bar, {width: width, height: height, x: x, y: y}, UNIT_ANIM_DUR, STD_EASING]
	let animElement = element.cloneNode(true);
	let newElement = element.cloneNode(true);

	for(var attributeName in props) {
		let animateElement;
		if(attributeName === 'transform') {
			animateElement = document.createElementNS("http://www.w3.org/2000/svg", "animateTransform");
		} else {
			animateElement = document.createElementNS("http://www.w3.org/2000/svg", "animate");
		}
		let currentValue = oldValues[attributeName] || element.getAttribute(attributeName);


		let value = props[attributeName];



		let animAttr = {
			attributeName: attributeName,
			from: currentValue,
			to: value,
			begin: "0s",
			dur: dur/1000 + "s",
			values: currentValue + ";" + value,
			keySplines: EASING[easingType],
			keyTimes: "0;1",
			calcMode: "spline",
			fill: 'freeze'
		};

		if(type) {
			animAttr["type"] = type;
		}

		for (var i in animAttr) {
			animateElement.setAttribute(i, animAttr[i]);
		}

		animElement.appendChild(animateElement);

		if(type) {
			newElement.setAttribute(attributeName, `translate(${value})`);
		} else {
			newElement.setAttribute(attributeName, value);
		}
	}

	return [animElement, newElement];
}

