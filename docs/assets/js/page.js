let $timeline = document.querySelector('.timeline');
let $makeTimeline = document.querySelector('.make-timeline');
let timelineInput = document.querySelector('.timeline-input');

var canvas = document.querySelector(".action-area");
let bar = document.querySelector('.test-bar');

let framesDict = {};
let frames = [];
// let current
var activeElement,
activeElementProps,
activeFrameIndex = 0,
$activeFrame,
currentFrameProps = [],
noOfFrames = 5,
//  TODO:
keyTimes = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
duration = '3000ms',
extractedNodes;

function forEachNode(nodeList, callback, scope) {
	if(!nodeList) return;
	for (var i = 0; i < nodeList.length; i++) {
		callback.call(scope, nodeList[i], i);
	}
}

function activate($parent, $child, commonClass, activeClass='active', index = -1) {
	let $children = $parent.querySelectorAll(`.${commonClass}.${activeClass}`);

	forEachNode($children, (node, i) => {
		if(index >= 0 && i <= index) return;
		node.classList.remove(activeClass);
	})

	$child.classList.add(activeClass);
}

let animatableProps = ['x', 'y', 'width', 'height', 'opacity', 'r', 'tranform'];

let overlayGroup = drawElement({
	properties: {},
	parent: canvas,
	tagName: 'g'
});

let boxConfig = {
	properties: {
		x: 0,
		y: 0,
		width: 0,
		height: 0,
		fill: 'none',
		stroke: 'dodgerblue',
		'stroke-dasharray': '5,5'
	},
	parent: overlayGroup,
	tagName: 'rect'
};
let rotateDotConfig = {
	properties: {
		cx: 0,
		cy: 0,
		r: 0,
		fill: "dodgerblue"
	},
	parent: overlayGroup,
	tagName: 'circle'
};

let scaleDotConfig = {
	properties: {
		cx: 0,
		cy: 0,
		r: 0,
		fill: "green"
	},
	parent: overlayGroup,
	tagName: 'circle'
};

let box = drawElement(boxConfig);
let rotateDot = drawElement(rotateDotConfig);
let scaleDot = drawElement(scaleDotConfig);



// let li = $timeline.querySelector("data-index='0'");
// let a = li.querySelector('a');


$makeTimeline.addEventListener('click', () => {

	let [d, f] = [document.querySelector('.duration-input'), document.querySelector('.frames-input')];
	duration = parseFloat(d.value);
	noOfFrames = parseInt(f.value);
	let timespan = duration / frames;

	let array = new Array(frames+1).fill(0);
	keyTimes = array.map((d, i) => {
		return timespan * i;
	});

	let lis = '';

	keyTimes.map((val, i) => {
		lis += getTimelineElement(val, i);
	})

	$timeline.innerHTML = lis;

	timelineInput.parentNode.removeChild(timelineInput);

	function getTimelineElement(value, i) {
		return `<li data-index="${i}"><a href="#" data-index="${i}">${value}</a><div class="preview"></div></li>`;
	}
});



Array.prototype.slice.call(
	document.querySelectorAll(".frame")
).map(element => {
	element.addEventListener('click', (e) => {
		updateFramesDict();
		console.log('framesDict', framesDict);

		let index = element.getAttribute("data-index");
		activeFrameIndex = index;

		activate($timeline, element, 'frame', 'active');

		// Array.prototype.slice.call(
		// 	$timeline.querySelectorAll('a')).map(el => {
		// 	el.classList.remove('active');
		// });
		// element.classList.add('active');

		// resetActionArea();

		if($activeFrame) {
			$activeFrame.classList.add('keyframe');
		}
		$activeFrame = element;
	});
});

function updateFramesDict() {
	framesDict[activeFrameIndex] = currentFrameProps;
}

// New Frame click
// $timeline.addEventListener('click', (e) => {
// 	let element = e.target;
// 	// console.log(element.tagName);
// 	if(element.tagName === 'A') {
// 		framesDict[activeFrameIndex] = currentFrameProps;


// 		let index = element.getAttribute("data-index");
// 		activeFrameIndex = index;

// 		Array.prototype.slice.call(
// 			$timeline.querySelectorAll('a')).map(el => {
// 			el.classList.remove('active');
// 		});
// 		element.classList.add('active');

// 		// resetActionArea();
// 	}
// });

// Crude
extractedNodes = canvas.childNodes;


updateCurrentFrameProps();

function updateCurrentFrameProps() {

	// currentFrameProps = [
	// 	{
	// 		type: 'rect',
	// 		keyTime: 0.1,
	// 		props: [
	// 			{
	// 				name: 'x',
	// 				value: '10px'
	// 			},
	// 			{
	// 				name: 'transform',
	// 				value: 'translate("0px", "10px")'
	// 			}
	// 		]
	// 	},
	// 	{

	// 	},
	// ]
	currentFrameProps = [];

	extractedNodes.forEach(element => {
		let o = {};
		o.type = element.tagName;
		o.keyTime = keyTimes[activeFrameIndex];

		o.props = [];

		if(!element.attributes || element.tagName ==='g') return;
		// if(!element.attributes && typeof (element.getAttribute) == 'function' && !element.getAttribute('transform')) return;
		// console.log(element, extractedNodes, document.getElementsByTagName('rect')[0].getAttribute('transform'));

		Object.values(element.attributes)
			.filter(attr => animatableProps.includes(attr.name) && attr.specified)
			.map(attr => {
				var prop = {
					name: attr.name,
					value: attr.nodeValue,
				};
				o.props.push(prop);
			});

		if(element.getAttribute('transform')) {
			o.props.push({
				name: 'tranform',
				value: element.getAttribute('transform')
			})
		}

		if(o.props.length) {
			currentFrameProps.push(o);
		}

	})
}

// console.log(overlayGroup);
overlayGroup.classList.add('hide');

document.querySelector('.play').addEventListener('click', (e) => {
	updateFramesDict();
	let container = canvas.parentNode;
	runSMILAnimation(container, canvas, ...buildData());

	// overlayGroup.classList.add('hide');
	// attachAnimElement();
});

function buildData() {
	// framesDict = {
	// 	'1': [
	// 		{
	// 			type: 'rect',
	// 			keyTime: 0.1,
	// 			props: [
	// 				{
	// 					name: 'x',
	// 					value: '10px'
	// 				},
	// 				{
	// 					name: 'transform',
	// 					value: 'translate("0px", "10px")'
	// 				}
	// 			]
	// 		},
	// 		{
	// 			type: 'circle',
	// 			keyTime: 0.1,
	// 			props: [
	// 				{
	// 					name: 'x',
	// 					value: '10px'
	// 				},
	// 				{
	// 					name: 'transform',
	// 					value: 'translate("0px", "10px")'
	// 				}
	// 			]
	// 		},
	// 	],

	// 	'5': [
	// 		{
	// 			type: 'rect',
	// 			keyTime: 0.5,
	// 			props: [
	// 				{
	// 					name: 'x',
	// 					value: '10px'
	// 				},
	// 				{
	// 					name: 'transform',
	// 					value: 'translate("0px", "10px")'
	// 				}
	// 			]
	// 		},
	// 		{
	// 			type: 'circle',
	// 			keyTime: 0.1,
	// 			props: [
	// 				{
	// 					name: 'x',
	// 					value: '10px'
	// 				},
	// 				{
	// 					name: 'transform',
	// 					value: 'translate("0px", "10px")'
	// 				}
	// 			]
	// 		},
	// 	]
	// }

	// framesArray = []

	var animElements = [];



	//to make individual animate elements for each prop of an element

	// var elementProps = {
	// 	'rect': {
	// 		'y': {
	// 			values: ['0px', '50px', '80px'],
	// 			keyTimes: [0, 0.3, 1],
	// 			dur: '3000ms'
	// 		},
	// 		'tranform': []
	// 	},
	// 	'circle': {
	// 		'cx': [],
	// 		'transform': []
	// 	}
	// };

	let allElements = {};
	extractedNodes.forEach(element => {
		if(element.tagName !== 'rect') return;
		allElements[element.tagName] = element;
	})

	var els = Object.values(framesDict)[0];
	var elementNames = els.map(d => d.type);

	console.log(els);

	var elementProps = {};
	for(var i = 0; i < els.length; i++) {
		var elementName = els[i].type;
		console.log(els[i]);
		var propNames = els[i].props.map(d => d.name);
		var o = {};
		propNames.map(name => {
			o[name] = {
				values: [],
				keyTimes: [],
				dur: ''
			};
		})

		Object.values(framesDict).map(d => {
			propNames.map((name, j) => {
				var value = d[i].props[j].value;
				o[name].values.push(value);
				o[name].keyTimes.push(d[i].keyTime);
				o[name].dur = duration;
			})
		});
		elementProps[elementName] = o;
	}

	return [elementProps, allElements];
}

function animateSVG(svgContainer, elementProps, allElements) {
	let animElements = [];
	Object.keys(elementProps).map(elName => {
		let props = elementProps[elName];
		let node = allElements[elName];

		let parent = node.parentNode;

		let animElem = animateSVGElement(node, props);
		parent.replaceChild(animElem, node);
	})

	return svgContainer.cloneNode(true);
}

function animateSVGElement(element, props) {
	let animElement = element.cloneNode(true);
	// let newElement = element.cloneNode(true);
	Object.keys(props).map(propName => {
		let animateElement;
		if(propName === 'transform') {
			animateElement = document.createElementNS("http://www.w3.org/2000/svg", "animateTransform");
		} else {
			animateElement = document.createElementNS("http://www.w3.org/2000/svg", "animate");
		}

		let o = props[propName];
		let animAttr = {
			attributeName: propName,
			// from: currentValue,
			// to: value,
			// begin: "0s",
			dur: o.dur,
			keyTimes: o.keyTimes.join(';'),
			values: o.values.join(';'),
			// keySplines: EASING[easingType],
			// calcMode: "spline",
			// fill: 'freeze',
			repeatCount: 1,
		};

		for (var i in animAttr) {
			animateElement.setAttribute(i, animAttr[i]);
		}

		animElement.appendChild(animateElement);

	});

	return animElement;
}

function runSMILAnimation(parent, svgElement, elementProps, allElements) {
	if(Object.keys(elementProps).length === 0) return;

	let animSvgElement = animateSVG(svgElement, elementProps, allElements);
	if(svgElement.parentNode == parent) {
		parent.removeChild(svgElement);
		parent.appendChild(animSvgElement);
	}
}














































var deg = 180 / Math.PI;
var rotating = false;
var dragging = false;
var impact = {
  x: 0,
  y: 0
};
var m = { //mouse
  x: 0,
  y: 0
};
var delta = {
  x: 0,
  y: 0
};

canvas.addEventListener('mousedown', (e) => {
	let element = e.target;
	// console.log('element', element);
	if(element.tagName === 'rect') {

		let elRect = element.getBoundingClientRect();
		let svgRect = canvas.getBoundingClientRect();
		let left = elRect.left - svgRect.left;
		let right = elRect.right - svgRect.left;
		let top = elRect.top - svgRect.top;
		let bottom = elRect.bottom - svgRect.top;

		let boxProps = {
			x: left,
			y: top,
			width: right - left,
			height: bottom - top
		};
		let rotateDotProps = {
			cx: right,
			cy: top,
			r: 6
		};
		let scaleDotProps = {
			cx: right,
			cy: bottom,
			r: 6
		}

		// console.log(right, top, bottom, element.getBoundingClientRect(), element.getAttribute('x'), element.getAttribute('y'));

		let elProps = {
			x: left,
			y: top
		}

		updateElement(box, boxProps);
		updateElement(rotateDot, rotateDotProps);
		updateElement(scaleDot, scaleDotProps);
		updateElement(element, elProps);

		activeElementProps = {
			el: element,
			// overlayGroup: overlayGroup,
			A: Math.atan2(elRect.height / 2, elRect.width / 2),
			a: 0,
			o: {
				x: 0,
				y: 0
			}
		};

		activeElement = element;

		dragging = 1;

		impact = oMousePos(canvas, e);
		delta.x = activeElementProps.o.x - impact.x;
		delta.y = activeElementProps.o.y - impact.y;

		if (e.target.tagName == "circle") {
			rotating = 1;
		}

	}
})

function update() {
	let ae = activeElementProps;
	var transf = 'translate(' + ae.o.x + ', ' + ae.o.y + ')' + ' rotate(' + (ae.a * deg) + ')';
	activeElement.setAttributeNS(null, 'transform', transf);
	// activeElement.
	overlayGroup.setAttributeNS(null, 'transform', transf);
	// box.setAttributeNS(null, 'transform', transf);
	// scaleDot.setAttributeNS(null, 'transform', transf);
	// rotateDot.setAttributeNS(null, 'transform', transf);
}

canvas.addEventListener("mouseup", function(e) {
	rotating = false;
	dragging = false;
	updateCurrentFrameProps()
  }, false);

canvas.addEventListener("mouseleave", function(e) {
	rotating = false;
	dragging = false;
}, false);

canvas.addEventListener("mousemove", function(e) {
	m = oMousePos(canvas, e);

	if (dragging) {
		activeElementProps.o.x = m.x + delta.x;
		activeElementProps.o.y = m.y + delta.y;
		update();
	}

	if (rotating) {
		// var index = rotating - 1;
		activeElementProps.a = Math.atan2(activeElementProps.o.y - m.y, activeElementProps.o.x - m.x) - activeElementProps.A;
		update();
	}
}, false);

// HELPERS

function oMousePos(svg, e) {
	var ClientRect = svg.getBoundingClientRect();
	return { //objeto
		x: Math.round(e.clientX - ClientRect.left),
		y: Math.round(e.clientY - ClientRect.top)
	}
}

function drawElement(o) {
	/*
	var o = {
		properties : {
		x1:100, y1:220, x2:220, y2:70},
		parent:document.querySelector("svg"),
		tagName:'line'
	}
	*/
	var SVG_NS = 'http://www.w3.org/2000/svg';
	var el = document.createElementNS(SVG_NS, o.tagName);
	for (var name in o.properties) {
		// console.log(name);
		if (o.properties.hasOwnProperty(name)) {
			el.setAttributeNS(null, name, o.properties[name]);
		}
	}
	o.parent.appendChild(el);
	return el;
}

function updateElement(el, properties) {
	for (var name in properties) {
		el.setAttribute(name, properties[name]);
	}
	return el;
}










var SVGLink_NS = 'http://www.w3.org/1999/xlink';
var SVG_NS = 'http://www.w3.org/2000/svg';
var svg = document.querySelector(".test-svg");

// var deg = 180 / Math.PI;
// var rotating = false;
// var dragging = false;
// var impact = {
//   x: 0,
//   y: 0
// };
// var m = { //mouse
//   x: 0,
//   y: 0
// };
// var delta = {
//   x: 0,
//   y: 0
// };
var ry = []; // elements array
var objectsRy = [];

var pentaPolygon = {
  properties: {
    fill: 'url(#trama)',
    points: "0,-70 66.57395614066074,-21.631189606246316 41.14496766047312,56.63118960624632 -41.144967660473114,56.63118960624632 -66.57395614066075,-21.63118960624631 -1.2858791391047208e-14,-70"
  },
  //parent: this.g,
  tagName: 'polygon',
  pos: {
    x: 400,
    y: 400
  }
}

objectsRy.push(pentaPolygon);

// var ellipsePath = {
//   properties: {
//     d: 'M-90,0 a90,50 0 1, 0 0,-1  z',
//     fill: 'url(#trama)'
//   },
//   //parent: this.g,
//   tagName: 'path',
//   pos: {
//     x: 370,
//     y: 150
//   }
// }
// objectsRy.push(ellipsePath);

function Element(o, index) {
	var SVG_NS = 'http://www.w3.org/2000/svg';
  this.g = document.createElementNS(SVG_NS, 'g');
  this.g.setAttributeNS(null, 'id', index);
  svg.appendChild(this.g);

  o.parent = this.g;

  this.el = drawElement(o);
  this.a = 0;
  this.tagName = o.tagName;
  this.elRect = this.el.getBoundingClientRect();
  this.svgRect = svg.getBoundingClientRect();
  this.Left = this.elRect.left - this.svgRect.left;
  this.Right = this.elRect.right - this.svgRect.left;
  this.Top = this.elRect.top - this.svgRect.top;
  this.Bottom = this.elRect.bottom - this.svgRect.top;

  this.LT = {
    x: this.Left,
    y: this.Top
  };
  this.RT = {
    x: this.Right,
    y: this.Top
  };
  this.LB = {
    x: this.Left,
    y: this.Bottom
  };
  this.RB = {
    x: this.Right,
    y: this.Bottom
  };
  this.c = {
    x: 0, //(this.elRect.width / 2) + this.Left,
    y: 0 //(this.elRect.height / 2) + this.Top
  };
  this.o = {
    x: o.pos.x,
    y: o.pos.y
  };

  this.A = Math.atan2(this.elRect.height / 2, this.elRect.width / 2);

  this.pointsValue = function() { // points for the box
    return (this.Left + "," + this.Top + " " + this.Right + "," + this.Top + " " + this.Right + "," + this.Bottom + " " + this.Left + "," + this.Bottom + " " + this.Left + "," + this.Top);
  }

  var box = {
    properties: {
      points: this.pointsValue(),
      fill: 'none',
      stroke: 'dodgerblue',
      'stroke-dasharray': '5,5'
    },
    parent: this.g,
    tagName: 'polyline'
  }
  this.box = drawElement(box);

  var leftTop = {
    properties: {
      cx: this.LT.x,
      cy: this.LT.y,
      r: 6,
      fill: "blue"
    },
    parent: this.g,
    tagName: 'circle'
  }

  this.lt = drawElement(leftTop);

  this.update = function() {
    var transf = 'translate(' + this.o.x + ', ' + this.o.y + ')' + ' rotate(' + (this.a * deg) + ')';
    this.el.setAttributeNS(null, 'transform', transf);
    this.box.setAttributeNS(null, 'transform', transf);
    this.lt.setAttributeNS(null, 'transform', transf);
  }
}

for (var i = 0; i < objectsRy.length; i++) {
  var el = new Element(objectsRy[i], i + 1);
  el.update();
  ry.push(el)
}

// EVENTS

svg.addEventListener("mousedown", function(evt) {

  var index = parseInt(evt.target.parentElement.id) - 1;
//   console.log(ry[index].A * deg)
  if (evt.target.tagName == ry[index].tagName) {
    dragging = index + 1;
    impact = oMousePos(svg, evt);
    delta.x = ry[index].o.x - impact.x;
    delta.y = ry[index].o.y - impact.y;
  }

  if (evt.target.tagName == "circle") {
    rotating = parseInt(evt.target.parentElement.id);
  }

}, false);

svg.addEventListener("mouseup", function(evt) {
  rotating = false;
  dragging = false;
}, false);

svg.addEventListener("mouseleave", function(evt) {
  rotating = false;
  dragging = false;
}, false);

svg.addEventListener("mousemove", function(evt) {
  m = oMousePos(svg, evt);

  if (dragging) {
    var index = dragging - 1;
    ry[index].o.x = m.x + delta.x;
    ry[index].o.y = m.y + delta.y;
    ry[index].update();
  }

  if (rotating) {
    var index = rotating - 1;
    ry[index].a = Math.atan2(ry[index].o.y - m.y, ry[index].o.x - m.x) - ry[index].A;
    ry[index].update();
  }
}, false);

// HELPERS

function oMousePos(svg, evt) {
  var ClientRect = svg.getBoundingClientRect();
  return { //objeto
    x: Math.round(evt.clientX - ClientRect.left),
    y: Math.round(evt.clientY - ClientRect.top)
  }
}

// function drawElement(o) {
//   /*
//   var o = {
//     properties : {
//     x1:100, y1:220, x2:220, y2:70},
//     parent:document.querySelector("svg"),
//     tagName:'line'
//   }
//   */
//   var el = document.createElementNS(SVG_NS, o.tagName);
//   for (var name in o.properties) {
//     console.log(name);
//     if (o.properties.hasOwnProperty(name)) {
//       el.setAttributeNS(null, name, o.properties[name]);
//     }
//   }
//   o.parent.appendChild(el);
//   return el;
// }

